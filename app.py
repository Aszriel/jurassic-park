from flask import Flask, render_template
import requests
import random

app = Flask(__name__)

@app.route('/')
def pageAcceuil():
    reponseJSON = requests.get('https://allosaurus.delahayeyourself.info/api/dinosaurs/')
    return render_template('acceuil.html', reponseJSON=reponseJSON.json())  


@app.route('/dinosaur/<slug_du_dinosaur>/')
def pageDetaille(slug_du_dinosaur):
    dataDinosaur = requests.get('https://allosaurus.delahayeyourself.info/api/dinosaurs/%s' %slug_du_dinosaur)
    allDinosaur = requests.get('https://allosaurus.delahayeyourself.info/api/dinosaurs/')
    listJSONDinosaur = allDinosaur.json()
    i = 0
    topDinosaurs = list()
    while i < 3:
        result = random.randint(0,6)
        topDinosaurs.append(listJSONDinosaur[result])
        i += 1
    return render_template('details.html', dataDinosaur=dataDinosaur.json(), topDinosaurs=topDinosaurs)

